import React, { useCallback } from 'react';
import type { NextPage } from 'next';
import Head from 'next/head';
import styles from '../styles/Home.module.css';
import { SubmitHandler, useForm } from 'react-hook-form';
import { ethers } from 'ethers';
import ExposeMind from '../contracts/ExposeMind.json';

type Inputs = {
  address: string,
  privateKey: string,
  name: string,
  position: string,
  description: string,
};

const CONTRACT_ID = "0xe7f1725E7734CE288F8367e1Bb143E90bb3F0512";

const Home: NextPage = () => {
  const {register, handleSubmit, formState: {errors}} = useForm<Inputs>();

  const onSubmit: SubmitHandler<Inputs> = async data => {
    console.log(data);
    const nodeProvider = 'http://127.0.0.1:8545/';
    const customHttpProvider = new ethers.providers.JsonRpcProvider(nodeProvider);
    const signer = new ethers.Wallet(data.privateKey, customHttpProvider);
    const contractAddress = CONTRACT_ID;
    const contract = new ethers.Contract(contractAddress, ExposeMind.abi, signer);
    const result = await contract.addMeToUsers(data.name, data.position, data.description);
    console.log("Withdraw result:", result);
  }

  const getUser = (async () => {
    const nodeProvider = 'http://127.0.0.1:8545/';
    const customHttpProvider = new ethers.providers.JsonRpcProvider(nodeProvider);
    const signer = new ethers.Wallet("0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80", customHttpProvider);
    const contractAddress = CONTRACT_ID;
    const contract = new ethers.Contract(contractAddress, ExposeMind.abi, signer);
    const result = await contract.users("0x70997970C51812dc3A010C7d01b50e0d17dc79C8");
    console.log("User:", result);
  });

  return (
    <div className={styles.container}>
      <Head>
        <title>ExposeMind</title>
        <meta name="description" content="ExposeMind application"/>
        <link rel="icon" href="/favicon.ico"/>
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          ExposeMind
        </h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          address
          <input defaultValue="0x70997970C51812dc3A010C7d01b50e0d17dc79C8" {...register("address", {required: true})} /><br/>
          {errors.address && <span>This field is required</span>}<br/>
          private key
          <input defaultValue="0x59c6995e998f97a5a0044966f0945389dc9e86dae88c7a8412f4603b6b78690d"{...register("privateKey", {required: true})} /><br/>
          {errors.privateKey && <span>This field is required</span>}<br/>
          Name
          <input {...register("name", {required: true})} /><br/>
          {errors.name && <span>This field is required</span>}<br/>
          Position
          <input {...register("position", {required: true})} /><br/>
          {errors.position && <span>This field is required</span>}<br/>
          Description
          <input {...register("description", {required: true})} /><br/>
          {errors.description && <span>This field is required</span>}<br/>
          <input type="submit"/>
        </form>
        <button onClick={getUser}>POKAŻ</button>
      </main>
    </div>
  );
};

export default Home;
