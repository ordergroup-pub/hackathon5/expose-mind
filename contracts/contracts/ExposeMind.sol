// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

// import "hardhat/console.sol";

contract ExposeMind {
    address public owner;
    uint public mentorsCount = 0;

    struct User {
        address adr;
        string name;
        string position;
        string description;
        bool isMentor;
        address[] acceptedBy;
    }

    mapping(address => User) public users;

    constructor() {
        owner = msg.sender;
        users[owner] = User(
            owner,
            "Owner",
            "Owner position",
            "Owner description",
            true,
            new address[](0)
        );
        mentorsCount += 1;
    }

    function addMeToUsers(
        string calldata name,
        string calldata position,
        string calldata description
    ) public {
        require(users[msg.sender].adr == address(0x0), "User already exists");
        users[msg.sender] = User(
            msg.sender,
            name,
            position,
            description,
            false,
            new address[](0)
        );
    }

    function verifyMentor(address mentor) public {
        require(users[msg.sender].isMentor == true, "Only mentor can verify other mentor");
        require(users[mentor].adr != address(0x0), "User doesn't exist");
        require(msg.sender != mentor, "You cannot verify yourself");
        require(users[mentor].isMentor == false, "User is already a mentor");
        User storage user = users[mentor];
        bool alreadyAccepted = false;
        for (uint i = 0; i < user.acceptedBy.length; i++) {
            if (user.acceptedBy[i] == msg.sender) {
                alreadyAccepted = true;
                break;
            }
        }
        require(alreadyAccepted == false, "You already accepted this user as a mentor");
        user.acceptedBy.push(msg.sender);
        // float is not fully supported by solidity
        if (user.acceptedBy.length * 100 >= mentorsCount * 100 / 2) {
            user.isMentor = true;
            mentorsCount += 1;
        }
    }
}
