import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";

describe("ExposeMind", function () {
  async function deployUsersFixture() {
    const [owner, otherAccount, ...restAccounts] = await ethers.getSigners();

    const ExposeMind = await ethers.getContractFactory("ExposeMind");
    const exposeMind = await ExposeMind.deploy();

    return { exposeMind, owner, otherAccount, restAccounts };
  }

  describe("Deployment", function () {
    it("Should set the right owner", async function () {
      const { exposeMind, owner } = await loadFixture(deployUsersFixture);
      expect(await exposeMind.owner()).to.equal(owner.address);
    });

    it("Should set the right number of mentors", async function () {
      const { exposeMind, owner } = await loadFixture(deployUsersFixture);
      expect(await exposeMind.mentorsCount()).to.equal(1);
    });

    it("Should add owner as mentor", async function () {
      const { exposeMind, owner } = await loadFixture(deployUsersFixture);
      const user = await exposeMind.users(owner.address);
      expect(user.adr).to.equal(owner.address);
      expect(user.name).to.equal("Owner");
      expect(user.isMentor).to.equal(true);
    });
  });

  describe("AddMeToUsers", function () {
    it("Should add sender as new user", async function () {
      const { exposeMind, otherAccount } = await loadFixture(deployUsersFixture);
      await exposeMind.connect(otherAccount)
        .addMeToUsers("Other", "Senior", "My description");
      const user = await exposeMind.users(otherAccount.address);
      expect(user.adr).to.equal(otherAccount.address);
      expect(user.name).to.equal("Other");
      expect(user.position).to.equal("Senior");
      expect(user.description).to.equal("My description");
      expect(user.isMentor).to.equal(false);
    });

    it("Should throw error on duplicate user", async function () {
      const { exposeMind, otherAccount } = await loadFixture(deployUsersFixture);
      const otherExposeMind = exposeMind.connect(otherAccount);
      await otherExposeMind.addMeToUsers(
        "Other", "Position", "Description"
      );
      let user = await exposeMind.users(otherAccount.address);
      expect(user.adr).to.equal(otherAccount.address);
      await expect(otherExposeMind.addMeToUsers(
        "Other", "Position", "Description"))
        .to.rejectedWith("User already exists");
    });
  });

  describe("VerifyMentor", function () {
    it("Should throw error if user is not mentor", async function () {
      const { exposeMind, owner, otherAccount } = await loadFixture(deployUsersFixture);
      await expect(
        exposeMind.connect(otherAccount).verifyMentor(owner.address))
        .to.rejectedWith("Only mentor can verify other mentor");
    });

    it("Should throw error if user doesn't exist", async function () {
      const { exposeMind, otherAccount } = await loadFixture(deployUsersFixture);
      await expect(
        exposeMind.verifyMentor(otherAccount.address))
        .to.rejectedWith("User doesn't exist");
    });

    it("Should throw error if user try to verify himself", async function () {
      const { exposeMind, owner } = await loadFixture(deployUsersFixture);
      await expect(
        exposeMind.verifyMentor(owner.address))
        .to.rejectedWith("You cannot verify yourself");
    });

    it("Should verify mentor if accepted by 50% other mentors", async function () {
      const { exposeMind, otherAccount, restAccounts } = await loadFixture(deployUsersFixture);
      const otherExposeMind = exposeMind.connect(otherAccount);
      await otherExposeMind.addMeToUsers(
        "Other", "Position", "Description"
      );
      let user = await exposeMind.users(otherAccount.address);
      expect(user.isMentor).to.equal(false);
      await exposeMind.verifyMentor(otherAccount.address);
      user = await exposeMind.users(otherAccount.address);
      expect(user.isMentor).to.equal(true);
      expect(await exposeMind.mentorsCount()).to.equal(2);

      const account3 = restAccounts[0];
      await exposeMind.connect(account3).addMeToUsers(
        "Other3", "Position", "Description"
      );
      await exposeMind.verifyMentor(account3.address);
      expect(await exposeMind.mentorsCount()).to.equal(3);

      const account4 = restAccounts[1];
      await exposeMind.connect(account4).addMeToUsers(
        "Other4", "Position", "Description"
      );
      await exposeMind.verifyMentor(account4.address);
      expect(await exposeMind.mentorsCount()).to.equal(3);
      expect((await exposeMind.users(account4.address)).isMentor).to.equal(false);
      await exposeMind.connect(otherAccount).verifyMentor(account4.address);
      expect(await exposeMind.mentorsCount()).to.equal(4);
      expect((await exposeMind.users(account4.address)).isMentor).to.equal(true);
    });

    it("Should throw error if user is already a mentor", async function () {
      const { exposeMind, otherAccount } = await loadFixture(deployUsersFixture);
      const otherExposeMind = exposeMind.connect(otherAccount);
      await otherExposeMind.addMeToUsers(
        "Other", "Position", "Description"
      );
      await exposeMind.verifyMentor(otherAccount.address);
      await expect(
        exposeMind.verifyMentor(otherAccount.address))
        .to.rejectedWith("User is already a mentor");
    });

    it("Should throw error if user already accepted the mentor", async function () {
      const { exposeMind, otherAccount, restAccounts } = await loadFixture(deployUsersFixture);
      await exposeMind.connect(otherAccount).addMeToUsers(
        "Other", "Position", "Description"
      );
      await exposeMind.verifyMentor(otherAccount.address);
      expect(await exposeMind.mentorsCount()).to.equal(2);

      const account3 = restAccounts[0];
      await exposeMind.connect(account3).addMeToUsers(
        "Other3", "Position", "Description"
      );
      await exposeMind.verifyMentor(account3.address);
      expect(await exposeMind.mentorsCount()).to.equal(3);

      const account4 = restAccounts[1];
      await exposeMind.connect(account4).addMeToUsers(
        "Other4", "Position", "Description"
      );
      await exposeMind.verifyMentor(account4.address);
      expect(await exposeMind.mentorsCount()).to.equal(3);
      const user = await exposeMind.users(account4.address);
      console.log(user["acceptedBy"])
      await expect(
        exposeMind.verifyMentor(account4.address))
        .to.rejectedWith("You already accepted this user as a mentor");
    });
  });
});
